#include <stdio.h>
#include <math.h>

__device__ double kosinus(double kut){return cos(kut);} //kompajler mi nije dao da pozivam cos iz global funk.
__global__ void komponentesile(double *a,double *b)
{
  const int id=blockIdx.y*gridDim.x*blockDim.y*blockDim.x+blockIdx.x*blockDim.y*blockDim.x+threadIdx.y*blockDim.x+threadIdx.x;

  const double pi= 3.14159265359;
  double povrsinatrake=((id+1)*(id+1)*pi)-(id*id*pi);
  double ljudiutraci;
 
  if(id!=7331) ljudiutraci=8*povrsinatrake;//8 ljudi po kvadratnom metru.                                                   
  else ljudiutraci=91532;
        
   
  b[id]= ljudiutraci*65*9.81*(0.5/0.1)*kosinus((id-0.5)/6371000); //rezultantna sila (svih traka) je zbroj projekcija sila svake trake na smjer djelovanja rezultantne sile - Ftrake * cos(kut). Kut je duljina luka / radius. Ftrake je masa * g * displacement(sažimanje?). Za displacement sam uzeo da je to koliko im se koljena sviju kad doskoče i pretpostavio da skaču sa pola metra visine pa se "skrate" za 10 cm kad doskoče. Uz prosječnu masu od 65 kg, masa trake je 65 * broj ljudi u traci.  
  a[id]=ljudiutraci;
}
