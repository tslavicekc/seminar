import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray as ga
import numpy as np
import pycuda.curandom
from pycuda.compiler import SourceModule

np.set_printoptions(threshold=np.nan)
mod = SourceModule(open("sem.cu").read())
silapotraci = mod.get_function("komponentesile")

def trake():
#računa broj potrebnih traka. Traka je širine 1m, dok se svi ljudi ne rasporede u trake računa se koliko ljudi stane u sljedeću traku.                                                                                

    pi= 3.14159265359
    povrsinatrake=0
    ukupnapovrsina=0
    count=1
    preostalo=1351000000
    while (preostalo>0   ):
   #     print("\n\n",count)
        povrsinatrake=(count*count*pi)-((count-1)*(count-1)*pi)
 #       print("povrsina trake ", povrsinatrake)
        ljudiutraci=8*povrsinatrake
  #      print("Ljudi u traci",ljudiutraci)
        preostalo-=ljudiutraci
        count +=1
        ukupnapovrsina += povrsinatrake
 #       print("ostalo ljudi ",preostalo)
#    print("\nukupna povrsina ",ukupnapovrsina)
    return count-1
n=trake()

print ("Stanovništvo ćemo rasporediti u ",n," pojasa.")
a=ga.empty(n, dtype=np.float64) # incijalizrati kao gpuarray
b=ga.empty_like(a) # incijalizrati kao gpuarray

# silapotraci(drv.Out(a),drv.Out(b),block=(47,13,1),grid=(4,3))
silapotraci(a,b,block=(47,13,1),grid=(4,3))
#print(a)
#print ("\n\n\n",b)
#gpub=ga.to_gpu(b)
#sila=ga.sum(gpub)
sila=ga.sum(b)
print("Sila je ",sila,"N","\nAkceleracija je",sila/5.97219e24,"m/s^2\nPretpostvavimo da sila djeluje jednu sekundu. Tada bi brzina zemlje (u smjeru u kojem je sila djelovala) nakon skoka bila ",sila/5.97219e24,"m/s što znači da bi se za 1 metar pomakla u ",(1/(sila/5.97219e24))/(3600*24*365),"godina")


